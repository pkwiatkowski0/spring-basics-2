package pl.sda.beans;

import java.util.List;

/**
 * Created by patry on 11.03.2017.
 */
public class Message {
    private String text;
    private List<String> logins;
    private int priority;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<String> getLogins() {
        return logins;
    }

    public void setLogins(List<String> logins) {
        this.logins = logins;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}

