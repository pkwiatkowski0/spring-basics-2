package pl.sda.beans;

public class Bean {

    private String name;
    private boolean testMethodCalled = true;
    private static Bean bean;

    public static Bean getInstance(){
        if (bean == null) {
            bean = new Bean();
        }
        return bean;
    }
    private Bean() {}    // sposob powyzej przedstawia przykladowe uzycie singletona (obiekt ma jedna instancje). w taki sposob spring dziala automatycznie jezeli nie zdefiniujemy beana inaczej!



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isTestMethodCalled() {
       return testMethodCalled;
    }
}
