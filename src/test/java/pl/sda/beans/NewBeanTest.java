package pl.sda;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.sda.beans.NewBean;

import javax.annotation.Resource;

/**
 * Created by patry on 11.03.2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring-config.xml"})
public class NewBeanTest {

    @Resource
    NewBean newBean;

    private int oldNumber;


    @After
    //ta metoda bedzie wykonywana po zakonczeniu kazdego testu w klasie NewBeanTest
    public void after() {
        newBean.setNumber(oldNumber);
    }

    @Before
    //ta metoda bedzie wykonywana przed każdym testem w klasie NewBeanTest. Chdzi o to ze tworzenie beana typu singleton zmiana jego wartosci ma zasieg globalny wiec obowiazuje wszedzie. Dlatego tez w testach ustawiamy odpowiednia wartosc po zakonczeniu kazdego testu
 public void before() {
        newBean.setNumber(1);
        newBean.getNumber();
    }
    @Test
    public void test() {
        Assert.assertNotNull(newBean);
        Assert.assertNotNull(newBean.getFirstBean());
        Assert.assertNotNull(newBean.getSecondBean());
        Assert.assertEquals(1, newBean.getNumber());
    }
}