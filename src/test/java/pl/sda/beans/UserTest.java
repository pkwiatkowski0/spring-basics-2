package pl.sda;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.sda.beans.User;

import javax.annotation.Resource;

/**
 * Created by patry on 11.03.2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring-config.xml"})
public class UserTest {

    @Resource //adnotujemy ze to jest zasob testowy
            User user; //wstrzykujemy klase User

    @Test
    public void testRoomBean() {
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getRoom());
        Assert.assertEquals(2, user.getRoom().getRoomCapacity());

    }

}