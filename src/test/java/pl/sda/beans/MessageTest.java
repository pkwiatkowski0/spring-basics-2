package pl.sda.beans;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

import static org.junit.Assert.*;

/**
 * Created by patry on 11.03.2017.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring-config.xml"})
public class MessageTest {

    @Resource
    Message message; //wstrzykujemy beana ktorego stworzylismy w xml

    @Test
    public void testGetLogins() throws Exception {

        Assert.assertNotNull(message);
        Assert.assertNotNull(message.getLogins());
        Assert.assertTrue(message.getLogins().size() == 2);
        Assert.assertEquals("anna.chmiel", message.getLogins().get(1));
    }


}