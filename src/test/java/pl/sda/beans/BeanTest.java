package pl.sda;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.sda.beans.Bean;

import javax.annotation.Resource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring-config.xml"})
public class BeanTest {
    @Qualifier("test")
    @Resource
    private Bean bean;  //wstrzykniecie beana

    @Qualifier("test2")
    @Resource
    private Bean bean2; //wstrzykniecie beana2

    @Test
    public void testIfBeanInjected() throws Exception {
        Assert.assertNotNull(bean);
        Assert.assertEquals("TestName", bean.getName());
    }
     @Test
    public void testSecondBean() throws Exception {
        Assert.assertNotNull(bean2);
        Assert.assertEquals("TestName2", bean2.getName());
    }
   @Test
    public void testInitMethod() throws Exception {
        Assert.assertNotNull(bean);
        Assert.assertTrue(bean.isTestMethodCalled());
    }


}